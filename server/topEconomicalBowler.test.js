const matches =require("./matches.json")
const deliveries =require("./deliveries.json")

const {economicalBowler}= require('./topEconomicalBowler.js')

test(' testing economical Bowler',()=>{
  let result=[
  'RN ten Doeschate',
  'J Yadav',
  'V Kohli',
  'R Ashwin',
  'S Nadeem',
  'Parvez Rasool',
  'MC Henriques',
  'Z Khan',
  'M Vijay',
  'GB Hogg'
]

  expect(economicalBowler(matches,deliveries)).toEqual(result)
})
    

test(' testing economical bowler for invalid arguments',()=>{
  expect(() => economicalBowler("",null).toThrow(Error))
}) 
    
function matchesWon(matches)
{

    if(typeof matches !== 'object')
    {
        throw new Error ("invalid Argument passed");
    }
const output=matches.reduce(function(acc,curr){

      if(acc.hasOwnProperty(curr.season))
      {
         ++ acc[curr.season];
      }
      else 
      {
          acc[curr.season]=1;
      }
       
      return acc;

    },{ })

    return output;
}
module.exports.matchesWon=matchesWon;

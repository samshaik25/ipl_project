function extraRuns(matches,deliveries)
 {
    if(typeof matches !== 'object' && typeof deliveries !=='object')
    {
        throw new Error ("invalid Argument passed");
    }
     const matchId2016=matches.map(x=>{
        if(x.season==2016)
           {
               return x.id
           }
        })
         .filter(filterId=>{
                  if(filterId!=undefined)
                        return filterId
                 })
                 
       const extraR = deliveries.reduce(function (acc,curr){
             matchId2016.forEach(element => {
                 
                let { bowling_team,match_id,extra_runs}=curr;
                if(element==match_id){

                 if(typeof bowling_team==undefined)
                 {
                     acc[bowling_team]={};
                 }
                 
                    acc[bowling_team]=  acc[bowling_team]+parseInt(extra_runs)  || parseInt(extra_runs)
                 }
                      
                })
             return acc;
         },{})
        return extraR;
}
     
 module.exports.extraRuns=extraRuns;
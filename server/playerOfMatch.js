
function playerOfMatch(matches)
{
    let manOfMatch={}
    matches.forEach(object=>{
           manOfMatch[object.season]=player(object.season)
    })


    function player(season)
    {
        let man= matches.reduce(function (acc,curr){
            if(curr.season==season)
            {
                if(acc[curr.player_of_match])
                {
                    acc[curr.player_of_match]+=1
                }
                else{
                    acc[curr.player_of_match]=1
                }
            }
            return acc;
        },{ })
      let mann=Object.keys(man).reduce((a, b) => (man[a] > man[b]) ? a : b);

    return mann;
    }
    return manOfMatch;
    
}

module.exports.playerOfMatch=playerOfMatch
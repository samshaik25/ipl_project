// dummy object
let myMatches=[{"season":"2017","team1":"Sunrisers Hyderabad","team2":"Royal Challengers Bangalore",
"winner":"Sunrisers Hyderabad"},
{"season":"2016","team1":"Kolkata Knight Riders","team2":"Royal Challengers Bangalore",
"winner":"Royal Challengers Bangalore"},
{"season":"2015","team1":"Chennai Super Kings","team2":"Rajasthan Royals",
"winner":"Chennai Super Kings"},
{"season":"2011","team1":"Chennai Super Kings","team2":"Kolkata Knight Riders",
"winner":"Kolkata Knight Riders"},
{"season":"2011","team1":"Kings XI Punjab","team2":"Pune Warriors",
"winner":"Pune Warriors"}]


const {matchesWonPerTeam}=require('./matchesWonPerTeamPerYear.js')

test('matches won per team',()=>{
 const output ={
   '2011': { 'Kolkata Knight Riders': 1, 'Pune Warriors': 1 },
   '2015': { 'Chennai Super Kings': 1 },
   '2016': { 'Royal Challengers Bangalore': 1 },
   '2017': { 'Sunrisers Hyderabad': 1 }
 }
 
    expect(matchesWonPerTeam(myMatches)).toEqual(output)
 
 })
 
 test('throw exception for invalid argument passed',()=>{
   expect(() => matchesWonPerTeam("")).toThrow(Error);
 })
 


function strikeRate(matches,deliveries)
{  

    let seasonId=[]
    let strikeRate={};

    for(let index=2008;index<2018;index++)
    {
        matches.forEach(element=>{
            if(element.season==index)
            {
            seasonId.push(element.id)
            }
        })
        strikeRate[index]=gettingScore(seasonId)
        seasonId=[]
    }   

    function  gettingScore(seasonId)
    {

            const runs=   deliveries.reduce(function(acc,curr)
                {      
                   seasonId.forEach(obj=>{

                         if( obj==curr.match_id){
                              
                             if(curr.batsman=="MS Dhoni"){

                                if(acc[curr.batsman])
                                 {

                                   acc[curr.batsman]+=parseInt(curr.batsman_runs)                           
                                 }
                                  else{

                                     acc[curr.batsman]=parseInt(curr.batsman_runs)
                                     }  
                             }    
                          }
                    })
                        return acc;
             },{ })
    
         
         
      
      for(let key in runs)
      {
        runs[key] =(runs[key]/countingBalls(key)) *100;
      }
      function countingBalls(player)
      {
        let balls=0;
        wideBalls=0;
        deliveries.forEach(object=>{

            seasonId.find(id=>{

                if(id==object.match_id)
                {
                    if(object.batsman==player)
                    {
                        balls++;
                          if(object.wide_runs!=0)
                          {
                              wideBalls++;
                          }
                    }
                    
                }
            })
            
        })
        return balls-wideBalls;
    }

        return runs  ;  
 }

return strikeRate;


}

module.exports.strikeRate=strikeRate
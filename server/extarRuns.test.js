//dummy deliveries
const myDeliveries=[{
    match_id: '1',
    batting_team: 'Royal Challengers Bangalore',
    bowling_team: 'Sunrisers Hyderabad',
    extra_runs: '1',
  
  },
  {
    match_id: '2',
    batting_team: 'Royal Challengers Bangalore',
    bowling_team: 'Pune Warriors',
    is_super_over: '0',
    extra_runs: '5',
  },
  {
    match_id: '3',
    batting_team: 'Rajasthan Royals',
    bowling_team: 'Kochi Tuskers Kera',
    extra_runs: '4',
  },
  {
    match_id: '4',
    batting_team: 'Royal Challengers Bangalore',
    bowling_team: 'Chennai Super Kings',
    extra_runs: '3',
  
  }]
  
  const myMatchesExtra=[{
    id: '1',
    season: '2016',
    team1: 'Royal Challengers Bangalore',
    team2: 'Sunrisers Hyderabad',
    },
    {
        id: '2',
        season: '2016',
        team1: 'Royal Challengers Bangalore',
        team2: 'Pune Warriors',
        
      },
      {
        id: '3',
        season: '2016',
    
        team1: 'Kolkata Knight Riders',
        team2: 'Chennai Super Kings',
        
      },
      {
        id: '4',
        season: '2016',
        team1: 'Kolkata Knight Riders',
        team2: 'Kochi tuskers',
        
      }
  ]
  
  
   const {extraRuns}=require('./extraRuns.js')
  
  test('testing extra runs',()=>{
   let result={
    'Sunrisers Hyderabad': 1,
    'Pune Warriors': 5,
    'Kochi Tuskers Kera': 4,
    'Chennai Super Kings': 3
  }
  
    expect(extraRuns(myMatchesExtra,myDeliveries)).toEqual(result)
  })


  test ('testing for undefined and null',()=>{
    expect(() => extraRuns(undefined,null).toThrow(Error))
  })


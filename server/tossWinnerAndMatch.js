function tossWinners(matches)
{

    let wonBoth=matches.reduce(function (acc,curr)
    {
          if(curr.toss_winner==curr.winner)
          {      if(acc[curr.winner])
            {
                acc[curr.winner]=++acc[curr.winner]
            }
            else 
            {
                acc[curr.winner]=1
            }
          }
    
          return acc;
    },{})
    return wonBoth; 

}
module.exports.tossWinners=tossWinners
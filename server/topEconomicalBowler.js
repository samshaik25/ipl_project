

function  economicalBowler(matches,deliveries)
{


    if(typeof matches !== 'object' && typeof deliveries !=='object')
    {
        throw new Error ("invalid Argument passed");
    }

    
    const matchId2015=matches.reduce(function(acc,curr){
        if(curr.season==2015)
        {
           acc.push(curr.id);
        }
        return acc
      },[])

     const bowlers=deliveries.reduce(function(acc,curr)
     {
        matchId2015.forEach(element=>{
            if(element==curr.match_id)
            {  
                if(acc[curr.bowler])
                {
                    acc[curr.bowler] += parseInt(curr.total_runs)
                }
                else 
                {
                    acc[curr.bowler] = parseInt(curr.total_runs)
                }

            }
        })
        return acc;
     },{ })
     
    for(let key in bowlers)
    {
        bowlers[key]= Math.round((bowlers[key]/overs(key)) *100)/100
    }
     
    function overs(key)
    {
        let balls=0;
        let wideB=0;
        deliveries.forEach(object=>{
            matchId2015.find(element=>{
                if(element==object.match_id)
            
                {
                    if(object.bowler==key)
                      {
                         balls++;
                           if(object.wide_runs!=0)
                             {
                               wideB++;
                              }
                      }
                 }
                
            })
        })

        return (balls-wideB)/6;
    }

    bowlersSorted = Object.keys(bowlers).sort((a, b) => bowlers[a] - bowlers[b]);
    
    const listOfTop=[]
    for(let k=0;k<10;k++)
    {
        listOfTop[k]=bowlersSorted[k]
    }
    return listOfTop;
} 
module.exports.economicalBowler=economicalBowler



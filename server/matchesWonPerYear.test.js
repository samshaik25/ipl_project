
 const matches =require('./matches.json')
 const deliveries =require("./deliveries.json")


 const { matchesWon}=require('./matchesWonPerYear.js')
test('matchesWon',()=>{
    const output={
        '2008': 58,
        '2009': 57,
        '2010': 60,
        '2011': 73,
        '2012': 74,
        '2013': 76,
        '2014': 60,
        '2015': 59,
        '2016': 60,
        '2017': 59
      };
    expect(matchesWon(matches)).toEqual(output)
})
 

 test('testing invalid',()=>{
     expect(() => matchesWon(null)).toThrow(Error);
   })



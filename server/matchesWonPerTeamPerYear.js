function matchesWonPerTeam(matches)
{   
   if(typeof matches !== 'object')
    {
        throw new Error ("invalid Argument passed");
    }
      const matchesWon =matches.reduce((obj,match)=>{
          let{season,winner}=match;
      if(obj[season]==undefined){
          obj[season]={};
      }
      obj[season][winner] = obj[season].hasOwnProperty(winner) ? ++obj[season][winner] : 1;
return obj
    },{})

 return matchesWon
}
module.exports.matchesWonPerTeam=matchesWonPerTeam;